package com.example.matei.laborator1.Laborator7;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.matei.laborator1.R;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;


public class CameraActivity extends AppCompatActivity {

    private Camera mCamera;
    private CameraPreview mPreview;
    private FrameLayout preview;
    private ImageView camera_img;
    private byte[] picture = null;

    private boolean downloadImage(Bitmap bitmap){
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName =  timeStamp + ".png";
        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOut = null;
        File file = new File(path, imageFileName);
        try {
            fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 70, fOut);
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            setPicture(data);
            final Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);
            boolean val = downloadImage(picture);
            if(val){
                Toast.makeText(getApplicationContext(),"Picture saved.",Toast.LENGTH_LONG).show();

            }else{
                Toast.makeText(getApplicationContext(),"ERROR: Picture cannot be saved.",Toast.LENGTH_LONG).show();
            }

            camera_img.setImageBitmap(rotateBitmap(picture, 90));
            preview.setVisibility(View.GONE);
            camera_img.setVisibility(View.VISIBLE);

        }
    };

    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return c;
    }

    public void setPicture(byte[] data) {
        picture = data;
    }

    private Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void previewListener() {
        mPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCamera.takePicture(null, null, mPicture);
                mPreview.setOnClickListener(null);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        camera_img = findViewById(R.id.camera_img);
        mCamera = getCameraInstance();
        Camera.Parameters params = mCamera.getParameters();
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        params.set("orientation", "portrait");
        params.set("jpeg-quality", 70);
        mCamera.setParameters(params);
        mPreview = new CameraPreview(this, mCamera);
        preview = findViewById(R.id.camera_preview);
        preview.addView(mPreview);
        previewListener();
    }

}

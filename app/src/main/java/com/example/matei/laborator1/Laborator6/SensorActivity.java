package com.example.matei.laborator1.Laborator6;

import android.app.Activity;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.matei.laborator1.R;

import java.util.ArrayList;
import java.util.List;

public class SensorActivity extends Activity implements SensorEventListener {
    private SensorManager mSensorManager;

    List<String> sensorDetails;
    ListView sensorList;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_sensor);

        sensorList = this.findViewById(R.id.sensorList);
        sensorDetails = new ArrayList<>();
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);

        List<Sensor> list = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        for(Sensor sensor: list){
            sensorDetails.add("Name: " + sensor.getName() + "Vendor: "+ sensor.getVendor() + "  | Version: " + sensor.getVersion());
        }

        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sensorDetails);
        sensorList.setAdapter(itemsAdapter);


    }



    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
    }
}

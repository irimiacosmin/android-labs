package com.example.matei.laborator1;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<Produs> implements View.OnClickListener {

    private ArrayList<Produs> dataSet;
    private Context mContext;


    public CustomAdapter(ArrayList<Produs> data, Context context) {
        super(context, R.layout.row_item, data);
        this.dataSet = data;
        this.mContext = context;

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Produs dataModel = getItem(position);
        final ViewHolder viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item, parent, false);
            viewHolder.txtName = convertView.findViewById(R.id.name);
            viewHolder.txtType = convertView.findViewById(R.id.type);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        viewHolder.txtName.setText(dataModel.getTitlu());
        if(MainActivity.showPrices){
            viewHolder.txtType.setText("$"+dataModel.getPret().toString());

        }else{
            viewHolder.txtType.setText("");
        }
        return convertView;
    }

    private static class ViewHolder {
        TextView txtName;
        TextView txtType;
    }


}


package com.example.matei.laborator1.Laborator5;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.matei.laborator1.R;

public class MyPreferenceFragment extends PreferenceFragment
{
    public static final String KEY_LIST_PREFERENCE = "sortPref";

    private CheckBoxPreference checkBoxPreference;
    private Context context;


    private void writeThemeToPrefs(Object object){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("sortAsc", (Boolean)object).apply();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        context = this.getActivity().getApplicationContext();
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.fragment_preference);
        checkBoxPreference = (CheckBoxPreference)getPreferenceScreen().findPreference(KEY_LIST_PREFERENCE);
        checkBoxPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                writeThemeToPrefs(o);
                return true;
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}
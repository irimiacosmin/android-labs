package com.example.matei.laborator1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class ProductActivity extends AppCompatActivity {

    TextView title;
    TextView description;
    TextView price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        title = findViewById(R.id.textView2);
        price = findViewById(R.id.textView3);
        description = findViewById(R.id.textView4);

        Intent intent = getIntent();
//        String myString = intent.getStringExtra(Intent.EXTRA_TEXT);
//        Toast.makeText(getApplicationContext(), myString, Toast.LENGTH_LONG).show();
               Produs produs = (Produs) intent.getSerializableExtra("produs");

        title.setText(produs.getTitlu());
        price.setText("$" + produs.getPret().toString());
        description.setText(produs.getDescriere());
    }
}

package com.example.matei.laborator1;

import java.io.Serializable;

/**
 * Created by matei on 27-Feb-19.
 */

public class Produs implements Serializable {
    String titlu;
    String descriere;
    Double pret;

    public Produs(String titlu, Double pret, String descriere) {
        this.titlu = titlu;
        this.descriere = descriere;
        this.pret = pret;
    }

    public Double getPret() {
        return pret;
    }

    public void setPret(Double pret) {
        this.pret = pret;
    }

    public String getTitlu() {
        return titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }
}

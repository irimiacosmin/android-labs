package com.example.matei.laborator1;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.matei.laborator1.Laborator5.MyPreferenceActivity;
import com.example.matei.laborator1.Laborator6.MyGpsClass;
import com.example.matei.laborator1.Laborator6.SensorActivity;
import com.example.matei.laborator1.Laborator7.CameraActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class MainActivity extends AppCompatActivity {

    static boolean showPrices = false;
    ListView listView;
    TextView textView;
    CustomAdapter adapter;
    ArrayList<Produs> listaDeProduse;
    Context context;
    private MyGpsClass gps;


    private boolean getValueFromPref(){
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(this);
        return pref.getBoolean("sortAsc", false);
    }

    private void sortMyList(Boolean sort){
        if(sort) {
            Collections.sort(listaDeProduse, new Comparator<Produs>() {
                @Override
                public int compare(Produs produs, Produs t1) {
                    return produs.getTitlu().compareTo(t1.getTitlu());
                }
            });
        }else{
            Collections.shuffle(listaDeProduse);
        }
        adapter.notifyDataSetChanged();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MainActivityPermissionsDispatcher.allPermWithPermissionCheck(this);
        context = this.getApplicationContext();
        listView = this.findViewById(R.id.list);
        textView = this.findViewById(R.id.textView);

        listaDeProduse = new ArrayList<>();
        listaDeProduse.add(new Produs("Super Smash Bros. Ultimate", 120.0, "For all the scale – 74 playable characters, 103 stages and countless non-playable characters, items and Spirits – the genius of Super Smash Bros: Ultimate is pretty simple: it’s the same game you grew up with, done better than ever before. It’s your favourite characters, your favourite stages, your favourite rules. It’s what’s kept you playing for almost 20 years. This is a tribute to your video gaming past. And it’s brilliant fun."));
        listaDeProduse.add(new Produs("Red Dead Redemption 2", 89.50, "Rockstar's massive open-world cowboy adventure is a sprawling, beautiful landscape full of things to do. Your actions ripple throughout the world and every back road and town is littered by short stories and diversions. It's often a slow contemplative experience (in a good way) and while some may find that pace not to their liking, it's a welcome departure from Rockstar's thrill-a-minute GTA games."));
        listaDeProduse.add(new Produs("Call of Duty: Black Ops 4", 170.0, "There's no single-player campaign in the latest Call of Duty outing, but the renewed focus on multiplayer has delivered what many believe is the best battle royale around. The combination of Call of Duty's tight, responsive controls and fast-paced action creates quick, intense and addictive games and while there's just a single map for its battle royale mode, called Blackout, at present, more are bound to follow."));
        listaDeProduse.add(new Produs("Thronebreaker: The Witcher Tales", 70.0, "A 2D, isometric RPG with a card-based battle mechanic doesn't some like a promising premise for a great game, but add a story in the The Witcher universe with development handled by RPG masters CD Projekt Red and suddenly things are looking up. The card battles, based on a much-enhanced version of Gwent from The Witcher III, are enthralling and the storytelling is outstanding. We only wish there was a Switch version so we could take it on the train."));
        listaDeProduse.add(new Produs("Marvel's Spider-Man", 43.0, "Games based on superheroes have a mixed history. There are several great Batman games, but Superman has an awful record and all previous efforts to do Spider-Man in a game didn't inspire. Enter Insomniac Games, who have finally broken that trend with a captivating open-world adventure. It finds a perfect balance in giving you a satisfying challenge, while also making you feel like a powerful hero, swinging from building to building with ease. It's exclusive to the PS4 and an exceedingly good reason to pick one up."));
        listaDeProduse.add(new Produs("Banner Saga 3", 19.0, "The third and final instalment in this cult favourite turn-based strategy RPG, Banner Saga 3 combines fiendishly clever strategy with a brilliant narrative full of beautifully animated cut scenes and characters you'll grow to love and hate. If you haven't dipped into the series before, it comes highly recommended provided you enjoy a healthy challenge."));
        listaDeProduse.add(new Produs("Detroit: Become Human", 100.0, "Quantic Dream has continued to evolve its signature genre of cinematic branching adventure games, and Detroit: Become Human is certainly its best so far. Playing as three different android protagonists, your choices can take the narrative of androids going rogue in different directions leading to several varied endings;"));
        listaDeProduse.add(new Produs("Pillars of Eternity II: Deadfire", 130.0, "The previous Pillars of Eternity showed that there was still an appetite for the older style of isometric RPGs, and Obsidian Entertainment was more than happy to cater for it again. "));
        listaDeProduse.add(new Produs("God of War", 100.0, "There’s been a lot of changes made to the new God of War when compared to the original PlayStation 2 and 3 games. More inspiration has been taken from RPGs, with the addition of side quests a branching world map and more upgrades for weapons and armour, Kratos’ son joins the fight with his own abilities, and the Greek setting’s been swapped for a chillier Norse one. "));
        listaDeProduse.add(new Produs("Sea of Thieves", 78.0, "Rare Studios’ pirating adventure is a bit of a short voyage at the moment, as it lacks enough varied content for long-term appeal. What is there, however, is enjoyable both on your own or with a crew of friends. "));
        listaDeProduse.add(new Produs("Shadow of the Colossus", 69.0, "Team Ico's giant-slaying classic from the Playstation 2 has been given a refresh for the PS4, courtesy of Bluepoint Games. In order to resurrect a cursed maiden, the player must roam the Forbidden Lands and defeat all 16 bosses who inhabit it, each encounter part-environmental-puzzle and part-combat-challenge. It's just as mysterious and fun to play as the original, but now looking better than ever."));
        listaDeProduse.add(new Produs("Into the Breach", 69.0, "From Subset Games, the creators of space-roaming rogue-like FTL: "));
        listaDeProduse.add(new Produs("Monster Hunter: World", 69.0, "The Monster Hunter series has finally got the mainstream."));
        listaDeProduse.add(new Produs("Iconoclasts", 69.0, "A one-man production eight years in the making."));


        adapter = new CustomAdapter(listaDeProduse, context);

        listView.setAdapter(adapter);
        sortMyList(getValueFromPref());

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Produs dataModel = listaDeProduse.get(position);
                textView.setText(dataModel.getDescriere());
                startProductActivity(dataModel);
            }
        });


    }

    private void startProductActivity(Produs produs){
        Intent intent = new Intent(this,ProductActivity.class);
        intent.putExtra("produs", (Serializable) produs);
        startActivity(intent);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("textView", textView.getText().toString());
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.containsKey("textView")) {
            String textViewValue = savedInstanceState.getString("textView");
            textView.setText(textViewValue);
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        sortMyList(getValueFromPref());
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return true;
    }

    private void togglePrices(){
        if(showPrices){
            adapter.notifyDataSetChanged();
            showPrices = false;

        }else{
            adapter.notifyDataSetChanged();
            showPrices = true;
        }
    }

    private void startSensorActivity(){
        Intent intent = new Intent(this, SensorActivity.class);
        startActivity(intent);
    }

    private void startCameraActivity(){
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }

    private void callMyGps(){
        gps = new MyGpsClass(MainActivity.this);

        // Check if GPS enabled
        if(gps.canGetLocation()) {

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            // \n is for new line
            Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        } else {
            // Can't get location.
            // GPS or network is not enabled.
            // Ask user to enable GPS/network in settings.
            gps.showSettingsAlert();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.showPrices)
        {
            togglePrices();
            return true;
        }else if(item.getItemId() == R.id.addProduct){
            showAddNewProductDialog();
            return true;
        }else if(item.getItemId() == R.id.preferences){
            Intent intent = new Intent(this, MyPreferenceActivity.class);
            startActivity(intent);
            return true;
        }else if(item.getItemId() == R.id.sensors){
            startSensorActivity();
            return true;
        }else if(item.getItemId() == R.id.camera){
            startCameraActivity();
            return true;
        }else if(item.getItemId() == R.id.gps){
            callMyGps();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private Produs makeProductFromDialog(String text, String description, String price){
        Produs produs = null;
        try{
            Double priceInDouble = Double.valueOf(price);
            if(text.length() > 3 || description.length() > 3){
                produs = new Produs(text, priceInDouble, description);
            }

        }catch (Exception e){}
        return produs;
    }

    public void showAddNewProductDialog() {
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View myDialogThatAddsItem = layoutInflater.inflate(R.layout.dialog_add, null);
        final EditText productName = (EditText) myDialogThatAddsItem.findViewById(R.id.productName);
        final EditText productPrice = (EditText) myDialogThatAddsItem.findViewById(R.id.productPrice);
        final EditText productDescription = (EditText) myDialogThatAddsItem.findViewById(R.id.productDescription);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(myDialogThatAddsItem)
                .setMessage("Add a new product")
                .setPositiveButton("Add product", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                       Produs produs = makeProductFromDialog(productName.getText().toString(), productDescription.getText().toString(),
                               productPrice.getText().toString());
                       if(produs!=null){
                           listaDeProduse.add(produs);
                           adapter.notifyDataSetChanged();
                           Toast.makeText(context, "Produs adaugat cu succes", Toast.LENGTH_LONG).show();
                       }else{
                           Toast.makeText(context, "Datele introduse nu pot alcatui un produs valid.", Toast.LENGTH_LONG).show();
                       }
                    }
                });
        Dialog myDialog =  builder.create();
        myDialog.show();
    }


    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void givMeStoregiPliz() {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }


    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void allPerm() {
    }
}
